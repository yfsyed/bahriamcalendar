//
//  ArticlesTableViewController.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/23/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import "ArticlesTableViewController.h"
#import "ArticleDetailsViewController.h"

@interface ArticlesTableViewController ()
@property (nonatomic,strong) NSArray *articleList;
@end

@implementation ArticlesTableViewController

-(NSArray *)articleList{
    if(!_articleList) _articleList = [[NSArray alloc]initWithObjects:
                                      @"Articles of Faith",
                                      @"Iman",
                                      @"Ihsaan",
                                      @"Zakat",
                                      @"The pathway to Allah", nil];
    return _articleList;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.articleList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"articleCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if(!cell) cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"articleCell"];
    // Configure the cell...
    cell.textLabel.text = [self.articleList objectAtIndex:indexPath.row];
    cell.tag=indexPath.row;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UITableViewCell *cell = (UITableViewCell *)sender;
    NSInteger index = cell.tag;
    if([segue.identifier isEqualToString:@"article"]){
        [segue.destinationViewController setArticleIndex:(int)index];
    }
}
@end
