//
//  Safar.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/17/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "SafarEventList.h"

@implementation SafarEventList
+(NSDictionary *) eventDictionary{
    
    NSString *secondDay = NSLocalizedString(@"Hzt Bandagi Miran Syed Yousuf RH Barah bani Israil",nil);

    NSArray *thirdArray =[[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Hzt Bandagi Miran Syed Yousuf RH Barah bani Israil",nil),
                               NSLocalizedString(@"Hzt BandagiMiyan Syed Ishaq RH",nil),nil];
    
    
    NSString *forthDay = NSLocalizedString(@"Hzt BandagiMiyan Syed Ishaq RH",nil);

    NSString *fifthDay = NSLocalizedString(@"Hzt Bandagi Miyan Shah Khalil Muhammad RH",nil);
    
    NSArray *sixthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Syed Zainul Abideen RH",nil),
                           NSLocalizedString(@"Hzt Bibi Aisha RZ",nil),nil];
    
    NSArray *seventhArray = [[NSArray alloc] initWithObjects:
                             NSLocalizedString(@"Hzt Bandagi Miyan Shah Ibrahim RH",nil),
                             NSLocalizedString(@"Hzt Imam Hasan RZ Shaheed",nil),
                             NSLocalizedString(@"Hzt Bandagi Miyan Syed Khuda Baksh RH",nil),nil];
    
    
    NSArray *eighthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Bandagi Miyan Shah Ibrahim RH",nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Najmuddin RH",nil),nil];
    
    NSString *ninthDay = NSLocalizedString(@"Hzt Imam Moosa Raza RH",nil);
    NSString *tenthDay = NSLocalizedString(@"Hzt Burhanuddin Aulia RH",nil);
    
    NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Roshan Miyan Sahab RH",nil),
                                NSLocalizedString(@"Hzt Miyan Syed Yakoob Urf Roshan Miyan Ahle Harood RH",nil),nil];
    
    NSString *fourteenthDay = NSLocalizedString(@"Hzt Syed Muhammad Abji Miyan Shaheed RH",nil);
    NSString *fifteenthDay = NSLocalizedString(@"Hzt Miyan Syed Shahabuddin Shaheed-e-Sidhot RH",nil);
    NSString *eighteenthDay = NSLocalizedString(@"Hzt Imam Syed Muhammad Baqar RH",nil);
    
    NSArray *ninteenthArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Hzt Bandagi Miyan Syed Esa RH ",nil),
                               NSLocalizedString(@"Hzt Miyan Syed Jalal RH Alias Babu Miyan Shaheed",nil),nil];
    
    NSString *twentythDay = NSLocalizedString(@"Hzt Syed Muhammad Momin Arif RH",nil);
    NSString *twentyOnethDay = NSLocalizedString(@"Hzt Miyan Syed Wali RH Musannif Sawanih Mahdi-e-Ma'ud AHS",nil);
    
    NSArray *twentyThirdArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Bandagi Miyan Syed Ghaisuddin RH",nil),
                                 NSLocalizedString(@"Hzt Miyan Syed Mahmood RH",nil),nil];
    
    NSArray *twentyFourthArray = [[NSArray alloc] initWithObjects:
                                  NSLocalizedString(@"Hzt Miyan Syed Ibrahim RH",nil),
                                  NSLocalizedString(@"Hzt Miyan Syed Zainul Abideen RH",nil),
                                  NSLocalizedString(@"Hzt Bandagi Miyan Syed Raj Muhammad RH",nil),nil];
    
    NSString *twentyFifthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Raj Muhammad RH",nil);
    NSString *twentySixthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Ayub RH",nil);
    
    NSString *twentySeventhDay = NSLocalizedString(@"Hzt Miyan Syed Ibrahim RH",nil);
    
    NSString *twentyEighthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Shah Nusrat Maqsus-us-Zaman RH",nil);
    NSString *twentyNinthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Shah Nusrat Maqsus-us-Zaman RH",nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects: secondDay, thirdArray, forthDay, fifthDay,
                       sixthArray, seventhArray, eighthArray, ninthDay, tenthDay,
                       thirteenthArray, fourteenthDay, fifteenthDay, eighteenthDay, ninteenthArray, twentythDay, twentyOnethDay,
                       twentyThirdArray, twentyFourthArray, twentyFifthDay, twentySixthDay,
                       twentySeventhDay, twentyEighthDay, twentyNinthDay, nil];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}


+(NSDictionary *) eventTypeDictionary{
    
    NSString *secondDay = NSLocalizedString(@"BAHRIAM",nil);
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                       NSLocalizedString(@"URS",nil),
                       NSLocalizedString(@"BAHRIAM", nil),nil];
    
    NSString *forthDay = NSLocalizedString(@"URS",nil);
    
    NSString *fifthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *sixthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"URS",nil),
                           NSLocalizedString(@"URS",nil),nil];
    
    NSArray *seventhArray = [[NSArray alloc] initWithObjects:
                             NSLocalizedString(@"BAHRIAM",nil),
                             NSLocalizedString(@"URS",nil),
                             NSLocalizedString(@"URS",nil),nil];
    
    
    NSArray *eighthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"URS",nil),
                            NSLocalizedString(@"URS",nil),nil];
    
    NSString *ninthDay = NSLocalizedString(@"URS",nil);
    NSString *tenthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"URS",nil),
                                NSLocalizedString(@"URS",nil),nil];
    
    NSString *fourteenthDay = NSLocalizedString(@"URS",nil);
    NSString *fifteenthDay = NSLocalizedString(@"URS",nil);
    NSString *eighteenthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *ninteenthArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"URS",nil),
                               NSLocalizedString(@"URS",nil),nil];
    
    NSString *twentythDay = NSLocalizedString(@"URS",nil);
    NSString *twentyOnethDay = NSLocalizedString(@"URS",nil);
    
    NSArray *twentyThirdArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"URS",nil),
                                 NSLocalizedString(@"URS",nil),nil];
    
    NSArray *twentyFourthArray = [[NSArray alloc] initWithObjects:
                                  NSLocalizedString(@"URS",nil),
                                  NSLocalizedString(@"URS",nil),
                                  NSLocalizedString(@"BAHRIAM",nil),nil];
    
    NSString *twentyFifthDay = NSLocalizedString(@"URS",nil);
    NSString *twentySixthDay = NSLocalizedString(@"URS",nil);
    
    NSString *twentySeventhDay = NSLocalizedString(@"URS",nil);
    
    NSString *twentyEighthDay = NSLocalizedString(@"BAHRIAM",nil);
    NSString *twentyNinthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects: secondDay, thirdArray, forthDay, fifthDay,
                       sixthArray, seventhArray, eighthArray, ninthDay, tenthDay,
                       thirteenthArray, fourteenthDay, fifteenthDay, eighteenthDay, ninteenthArray, twentythDay, twentyOnethDay,
                       twentyThirdArray, twentyFourthArray, twentyFifthDay, twentySixthDay,
                       twentySeventhDay, twentyEighthDay, twentyNinthDay, nil];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects:
                     @"02", @"03", @"04", @"05",
                     @"06", @"07", @"08", @"09", @"10",
                     @"13", @"14", @"15", @"18", @"19", @"20", @"21",
                     @"23", @"24", @"25", @"26",
                     @"27", @"28", @"29", nil];
    return keys;
}

@end
