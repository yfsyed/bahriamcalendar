//
//  MuharramEventsList.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/23/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MuharramEventsList : NSObject
+(NSDictionary *) eventDictionary;
+(NSDictionary *) eventTypeDictionary;
+(NSArray *) eventsKeys;
@end
