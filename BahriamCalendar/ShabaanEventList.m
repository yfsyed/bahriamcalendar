//
//  ShabaanEventList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/18/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "ShabaanEventList.h"

@implementation ShabaanEventList

+(NSDictionary *) eventDictionary{
    NSString *firstDay = NSLocalizedString(@"Hzt Miyan Syed Yousuf RH", nil);
    NSString *secondDay = NSLocalizedString(@"Hzt Bibi Fatima RZ D/o Mehdi-e-Ma'ud AHS", nil);
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Umm-al-Momineen Bibi Hafsa RZ", nil),
                           NSLocalizedString(@"Hzt Soban Suri RH", nil),nil];
    
    NSArray *forthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Sheikh Abul Qair Chisti RH Mubashir-e-Mehdi-e-Ma'ud AHS", nil),
                           NSLocalizedString(@"Hzt Miyan Syed Abdullah Saida Saheb RH", nil),nil];
    
    NSArray *sixthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Umm-e-Kulsum RZ D/o Hzt Rasool Allah SAS", nil),
                           NSLocalizedString(@"Hzt Bandagi Miyan Syed Murtuza RH", nil),nil];
    
    NSString *seventhDay = NSLocalizedString(@"Hzt Yousuf AHS", nil);
    
    NSArray *eighthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Bibi Bu-al-Fatah d/o Shahab-ul-Haq RZ", nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Abdul Shukoor", nil),nil];
    
    NSArray *ninthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Miyan Syed Ali RH", nil),
                           NSLocalizedString(@"Hzt Miyan Syed Moosa RH", nil),nil];
    
    NSArray *tenthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Nooh AHS", nil),
                           NSLocalizedString(@"Hzt Umm-al-Mumineen Bibi Aisha RZ", nil),nil];
    
    NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Miyan Syed Mohammad Taqi RH", nil),
                                NSLocalizedString(@"Hzt Miyan Syed Wali RH", nil),nil];
    
    NSArray *fourteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"General Shab-e-Baraat", nil),
                                NSLocalizedString(@"Bahr-e-Aam Bandagi Miyan Shah Abdul Kareem Noori RH", nil),
                                NSLocalizedString(@"Hzt Miyan Syed Sheikh Ismail", nil),
                                NSLocalizedString(@"Hzt Miyan Syed Allah Baksh RH", nil),nil];
    
    NSString *fifteenthDay = NSLocalizedString(@"Hzt Bandagi Miyan Shah Abdul Kareem Noori RH", nil);
    NSString *sixteenthDay = NSLocalizedString(@"Hzt Syed Yahiya RH wa Hzt Miyan Syed Esa RH Shaheedan Dantiwada", nil);
    NSString *seventeenthDay = NSLocalizedString(@"Hzt Miyan Shah Shareef Mohammad RH", nil);
    NSString *eighteenthDay = NSLocalizedString(@"Hzt Bayazid Bustami RH", nil);
    NSString *nineteenthDay = NSLocalizedString(@"Hzt Syed Peer Mohammad RH(Muallif Chirag-e-Deen-e-Nabavi)", nil);
    
    NSString *twentyFirstDay = NSLocalizedString(@"Bahr-e-Aam Hzt Bandagi Miyan Shah-e-Niamat RZ", nil);
    
    NSArray *twentySecondArray = [[NSArray alloc] initWithObjects:
                                  NSLocalizedString(@"Hzt Bandagi Miyan Shah-e-Niamat RZ", nil),
                                  NSLocalizedString(@"Hzt Bandagi Miyan Shah Abdul Lateef RH", nil),nil];
    
    NSString *twentyThirdDay = NSLocalizedString(@"Hzt Miyan Syed Jafer RH", nil);
    NSString *twentyForthDay = NSLocalizedString(@"Hzt Syed Baqar Bagha Miyan Saheb RH", nil);
    
    NSArray *twentySixthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Zul-Noon Misri RH 244H", nil),
                                 NSLocalizedString(@"Hzt Miyan Syed Jalal RH", nil),nil];
    
    NSString *twentySeventhDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Meeranji RH", nil);
    NSString *twentyNinthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Qasim RH", nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       firstDay, secondDay, thirdArray, forthArray, sixthArray, seventhDay, eighthArray, ninthArray, tenthArray,
                       thirteenthArray, fourteenthArray, fifteenthDay, sixteenthDay,seventeenthDay, eighteenthDay, nineteenthDay,
                       twentyFirstDay, twentySecondArray,twentyThirdDay,twentyForthDay,twentySixthArray, twentySeventhDay, twentyNinthDay,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;

}

+(NSDictionary *) eventTypeDictionary{
    NSString *firstDay = NSLocalizedString(@"Urs", nil);
    NSString *secondDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs", nil),
                           NSLocalizedString(@"Urs", nil),nil];
    
    NSArray *forthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs", nil),
                           NSLocalizedString(@"Urs", nil),nil];
    
    NSArray *sixthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs", nil),
                           NSLocalizedString(@"Urs", nil),nil];
    
    NSString *seventhDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *eighthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs", nil),
                            NSLocalizedString(@"Urs", nil),nil];
    
    NSArray *ninthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs", nil),
                           NSLocalizedString(@"Urs", nil),nil];
    
    NSArray *tenthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs", nil),
                           NSLocalizedString(@"Urs", nil),nil];
    
    NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Urs", nil),
                                NSLocalizedString(@"Urs", nil),nil];
    
    NSArray *fourteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"General", nil),
                                NSLocalizedString(@"Bahr-e-Aam", nil),
                                NSLocalizedString(@"Urs", nil),
                                NSLocalizedString(@"Urs", nil),nil];
    
    NSString *fifteenthDay = NSLocalizedString(@"Urs", nil);
    NSString *sixteenthDay = NSLocalizedString(@"Urs", nil);
    NSString *seventeenthDay = NSLocalizedString(@"Urs", nil);
    NSString *eighteenthDay = NSLocalizedString(@"Urs", nil);
    NSString *nineteenthDay = NSLocalizedString(@"Urs", nil);
    
    NSString *twentyFirstDay = NSLocalizedString(@"Bahr-e-Aam", nil);
    
    NSArray *twentySecondArray = [[NSArray alloc] initWithObjects:
                                  NSLocalizedString(@"Urs", nil),
                                  NSLocalizedString(@"Urs", nil),nil];
    
    NSString *twentyThirdDay = NSLocalizedString(@"Urs", nil);
    NSString *twentyForthDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *twentySixthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs", nil),
                                 NSLocalizedString(@"Urs", nil),nil];
    
    NSString *twentySeventhDay = NSLocalizedString(@"Urs", nil);
    NSString *twentyNinthDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       firstDay, secondDay, thirdArray, forthArray, sixthArray, seventhDay, eighthArray, ninthArray, tenthArray,
                       thirteenthArray, fourteenthArray, fifteenthDay, sixteenthDay,seventeenthDay, eighteenthDay, nineteenthDay,
                       twentyFirstDay, twentySecondArray,twentyThirdDay,twentyForthDay,twentySixthArray, twentySeventhDay, twentyNinthDay,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;

}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects:
                     @"01", @"02", @"03", @"04", @"06", @"07", @"08", @"09", @"10",
                     @"13", @"14", @"15", @"16", @"17", @"18", @"19",
                     @"21", @"22", @"23", @"24", @"26", @"27", @"29", nil];
    return keys;
}
@end
