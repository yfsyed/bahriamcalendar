//
//  RajjabEventList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/18/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "RajjabEventList.h"

@implementation RajjabEventList



+(NSDictionary *) eventDictionary{
    NSString *firstDay = NSLocalizedString(@"Hzt Imam Shafai'e RH", nil);
    NSString *secondDay = NSLocalizedString(@"Hzt Miyan Syed Manju RH", nil);
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Khwaja Shamsuddin Shirazi RH", nil),
                           NSLocalizedString(@"Hzt Imam Taqi RH", nil),
                           NSLocalizedString(@"Hzt Bandagi Miyan Syed Ali Sutun-e-deen RH", nil),nil];
    
    NSArray *forthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Umm-ul-Momineen Habiba RZ", nil),
                           NSLocalizedString(@"Hzt Moosa Kazim RH", nil),
                           NSLocalizedString(@"Hzt Bandagi Miyan Syed Ali Sutun-e-deen RH", nil),nil];
    
    NSString *fifthDay = NSLocalizedString(@"Hzt Khwaja Hasan Basri RH", nil);
    NSString *sixthDay = NSLocalizedString(@"Hzt Khwaja Moinuddin Chushti RH Ghareeb Nawaz", nil);
    NSString *seventhDay = NSLocalizedString(@"Hzt Bandagi Miyan Shaik Momin Tawakuli RZ", nil);
    NSString *ninthDay = NSLocalizedString(@"Hzt Ibrahim RZ s/o Hzt Rasool Allah SAS", nil);
    NSString *eleventhDay = NSLocalizedString(@"Hzt Murshid-uz-Zaman Syed Meeranji RH", nil);
    
    NSArray *twelveArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Bibi Zainab RZ", nil),
                            NSLocalizedString(@"Hzt Abbas RH", nil),nil];
    
	NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Syeduna Ali RZ", nil),
                                NSLocalizedString(@"Hzt Bandagi Miyan Abdul Latif RH", nil),nil];
    
    NSString *fourteenthDay = NSLocalizedString(@"Hzt Miyan Syed Osman RH", nil);
    
    NSArray *fifteenthArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Hzt Imam Jafar Sadiq RH", nil),
                               NSLocalizedString(@"Hzt Imam Azam RH", nil),
                               NSLocalizedString(@"Hzt Miyan Syed Shahabuddin RH", nil),nil];
    
    NSString *sixteenthDay = NSLocalizedString(@"Hzt Ibrahim Khalilullah AHS", nil);
    
    NSArray *seventeenthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Bibi Ruqiya RZ", nil),
                                 NSLocalizedString(@"Hzt Syed Qasim RH", nil),nil];
    
    NSString *nineteenthDay = NSLocalizedString(@"Mubarak Hzt Bandagi Miyan Sheikh Bheek RZ", nil);
    
    NSArray *twentyArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Umm-ul-Momineen Bibi Zainab RZ", nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Hameed Shaheed RH", nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Waliji RH", nil),nil];
    
    NSString *twentyThirdDay = NSLocalizedString(@"Hzt Bibi Amtul Raheem RH", nil);
    NSString *twentyForthDay = NSLocalizedString(@"Hzt Abul Hasan Khaisari RH", nil);
    NSArray *twentySixthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Shab-e-Miraj", nil),
                                 NSLocalizedString(@"Hzt Rangrez Shuhada RZ", nil),
                                 NSLocalizedString(@"Hzt Bibi Kad Banu RH", nil),
                                 NSLocalizedString(@"Hzt Bibi Aisha RH", nil),
                                 NSLocalizedString(@"Hzt Bandagi Miyan Syed Mustafa RH", nil), nil];
    
    NSString *twentySeventhDay = NSLocalizedString(@"Hzt Khwaja Junaid Baghdadi RH", nil);
    
    NSArray *twentyEighthArray = [[NSArray alloc] initWithObjects:
                                  NSLocalizedString(@"Hzt Miyan Syed Mubarak RH", nil),
                                  NSLocalizedString(@"Bandagi Miyan Roshan Munawwar RH", nil),nil];
    
    NSArray *twentyNinthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Bandagi Miyan Syed Abdul Hai Al-Mubashir Roshan Munawwar RZ", nil),
                                 NSLocalizedString(@"Hzt Syed Ameer RH", nil), nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       firstDay, secondDay, thirdArray, forthArray, fifthDay,sixthDay, seventhDay, ninthDay,
                       eleventhDay,twelveArray,thirteenthArray, fourteenthDay, fifteenthArray, sixteenthDay,seventeenthArray,nineteenthDay,
                       twentyArray,twentyThirdDay,twentyForthDay,twentySixthArray,
                       twentySeventhDay, twentyEighthArray,twentyNinthArray,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+(NSDictionary *) eventTypeDictionary{
    NSString *firstDay = NSLocalizedString(@"Urs", nil);
    NSString *secondDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs", nil),
                           NSLocalizedString(@"Urs", nil),
                           NSLocalizedString(@"Urs", nil),nil];
    
    NSArray *forthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs", nil),
                           NSLocalizedString(@"Urs", nil),
                           NSLocalizedString(@"Urs", nil),nil];
    
    NSString *fifthDay = NSLocalizedString(@"Urs", nil);
    NSString *sixthDay = NSLocalizedString(@"Urs", nil);
    NSString *seventhDay = NSLocalizedString(@"Urs", nil);
    NSString *ninthDay = NSLocalizedString(@"Urs", nil);
    NSString *eleventhDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *twelveArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs", nil),
                            NSLocalizedString(@"Urs", nil),nil];
    
	NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Viladat", nil),
                                NSLocalizedString(@"Urs", nil),nil];
    
    NSString *fourteenthDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *fifteenthArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Urs", nil),
                               NSLocalizedString(@"Urs", nil),
                               NSLocalizedString(@"Urs", nil),nil];
    
    NSString *sixteenthDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *seventeenthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs", nil),
                                 NSLocalizedString(@"Urs", nil),nil];
    
    NSString *nineteenthDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *twentyArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs", nil),
                            NSLocalizedString(@"Urs", nil),
                            NSLocalizedString(@"Urs", nil),nil];
    
    NSString *twentyThirdDay = NSLocalizedString(@"Urs", nil);
    NSString *twentyForthDay = NSLocalizedString(@"Urs", nil);
    NSArray *twentySixthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"General", nil),
                                 NSLocalizedString(@"Urs", nil),
                                 NSLocalizedString(@"Urs", nil),
                                 NSLocalizedString(@"Urs", nil),
                                 NSLocalizedString(@"Urs", nil), nil];
    
    NSString *twentySeventhDay = NSLocalizedString(@"Urs", nil);
    
    NSArray *twentyEighthArray = [[NSArray alloc] initWithObjects:
                                  NSLocalizedString(@"Urs", nil),
                                  NSLocalizedString(@"Bahr-e-Aam", nil),nil];
    
    NSArray *twentyNinthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs", nil),
                                 NSLocalizedString(@"Urs", nil), nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       firstDay, secondDay, thirdArray, forthArray, fifthDay,sixthDay, seventhDay, ninthDay,
                       eleventhDay,twelveArray,thirteenthArray, fourteenthDay, fifteenthArray, sixteenthDay,seventeenthArray,nineteenthDay,
                       twentyArray,twentyThirdDay,twentyForthDay,twentySixthArray,
                       twentySeventhDay, twentyEighthArray,twentyNinthArray,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects:
                     @"01", @"02", @"03", @"04", @"05", @"06", @"07", @"09",
                     @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"19",
                     @"20", @"23", @"24", @"26", @"27", @"28", @"29", nil];
    return keys;
}
@end
