//
//  ArticleDetailsViewController.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/23/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import "ArticleDetailsViewController.h"

@interface ArticleDetailsViewController ()

@end

@implementation ArticleDetailsViewController

-(void)setArticleIndex:(int)articleIndex{
    _articleIndex = articleIndex;
}

-(void)loadArticle{
    NSString *filePath;
    NSURL *url;
    switch(self.articleIndex){
        case IHSAAN:
            filePath = [[NSBundle mainBundle] pathForResource:@"Ihsan" ofType:@"pdf"];
            break;
        case IMAN:
            filePath = [[NSBundle mainBundle] pathForResource:@"Iman" ofType:@"pdf"];
            break;
        case ZAKAT:
            filePath = [[NSBundle mainBundle] pathForResource:@"Zakat" ofType:@"pdf"];
            break;
        case ARTICLES_OF_FAITH:
            filePath = [[NSBundle mainBundle] pathForResource:@"articles_of_faith" ofType:@"pdf"];
            break;
        case THE_PATHWAY_TO_ALLAH:
            filePath = [[NSBundle mainBundle] pathForResource:@"The_pathway_to_Allah" ofType:@"pdf"];
           
            break;

            
    }
    url = [NSURL fileURLWithPath:filePath];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.myWebView loadRequest:urlRequest];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadArticle];
    UINavigationController *navCon  = (UINavigationController*)[self.navigationController.viewControllers objectAtIndex:1];
    navCon.navigationItem.title = [self getCurrentArticleName];
}

-(NSString *)getCurrentArticleName{
    NSString *articleName;
    switch(self.articleIndex){
        case ARTICLES_OF_FAITH:
            articleName = @"Articles of Faith";
            break;
        case THE_PATHWAY_TO_ALLAH:
            articleName = @"The pathway to Allah";
            break;
        case IMAN:
            articleName = @"Iman";
            break;
        case IHSAAN:
            articleName = @"Ihsaan";
            break;
        case ZAKAT:
            articleName = @"Zakat";
            break;
    }
    return articleName;
}
@end
