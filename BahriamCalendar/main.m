//
//  main.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/20/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MICCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MICCAppDelegate class]));
    }
}
