//
//  MuharramEventsList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/23/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import "MuharramEventsList.h"


@implementation MuharramEventsList
+(NSDictionary *) eventDictionary{
    
    NSArray *dayOne = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"ISLAMIC NEW YEAR",nil),
                            NSLocalizedString(@"Amir-ul-Momineen Hzt Omar-e-Farooq(RZ)", nil),
                            nil];
    
    NSString *forthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Ruhullah(RH)",nil);
    NSString *fifthDay = NSLocalizedString(@"Hzt Sheikh Farid Gunj Shakar Farooqi(RH)",nil);
    NSString *sixthDay = NSLocalizedString(@"Hzt Bandagi Miyan Malik Barqur'dar, alias Malik Baqan(RZ)",nil);
    NSString *eighthDay = NSLocalizedString(@"Hzt Miyan Syed Ishaq(RH)",nil);
    NSString *tenthDay = NSLocalizedString(@"AASHURA, Martyrdom Of  Hzt Syeduna Imam Hussain(RZ) with companions",nil);
    NSString *eleventhDay = NSLocalizedString(@"Hzt Miyan Syed Ibrahim(RH)",nil);
    
    NSArray *thirteenthArray =[[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Ashraf(RH)",nil),
                            NSLocalizedString(@"Hzt Chand Bibi Sahaba(RH)",nil),nil];
    NSString *fourteenDay = NSLocalizedString(@"Hzt Bandagi Meeran Syed Mahmood Syedanji Khatimul-Murshideen(RZ)",nil);
    NSString *fifteenthDay = NSLocalizedString(@"Hzt Bandagi Meeran Syed Mahmood Syedanji Khatimul-Murshideen(RZ)",nil);
    
    NSArray *seventeenthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Bandagi Miyan Syed Ibrahim(RZ)",nil),
                                 NSLocalizedString(@"Hzt Bandagi Miyan Syed Noor Muhammad Khatim-e-Kaar(RH)",nil),
                                 NSLocalizedString(@"Hzt Bandagi Miyan Syed Miran(RH)",nil),
                                 NSLocalizedString(@"Hzt Bandagi Miyan Syed Shah-e-Qasim(RH) Mujtahid-e-Grouh-e-Mahdavia",nil),nil];
    NSString *eighteenDay =NSLocalizedString(@"Hzt Bandagi Miyan Syed Abdul Majeed Noor Noush(RZ)",nil);
    NSArray *twentysecondArray = [[NSArray alloc] initWithObjects:
                                  NSLocalizedString(@"Hzt Imam Hasan Askari(RZ)",nil),
                                  NSLocalizedString(@"Hzt Bandagi Miyan Ameen Muhammad(RZ)",nil),nil];
    NSString *twentySixthDay = NSLocalizedString(@"Bheraul-Uloom Allama Syed Ashraf Shamsi Yadullahi(RH)",nil);
    NSString *twentyEightthDay = NSLocalizedString(@"Hzt Lad Khan Bibi Sahaba(RH)",nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects: dayOne, forthDay, fifthDay,
                       sixthDay, eighthDay, tenthDay, eleventhDay,
                       thirteenthArray, fourteenDay, fifteenthDay, seventeenthArray, eighteenDay,
                       twentysecondArray, twentySixthDay, twentyEightthDay, nil];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+(NSDictionary *) eventTypeDictionary{
    
    NSArray *dayOne = [[NSArray alloc] initWithObjects:
                        NSLocalizedString(@"General",nil),NSLocalizedString(@"BAHRIAM", nil),nil];
    NSString *forthDay = NSLocalizedString(@"URS",nil);
    NSString *fifthDay = NSLocalizedString(@"URS",nil);
    NSString *seventhDay = NSLocalizedString(@"URS",nil);
    NSString *eighthDay = NSLocalizedString(@"URS",nil);
    NSString *tenthDay = NSLocalizedString(@"Martyrdom",nil);
    NSString *eleventhDay = NSLocalizedString(@"URS",nil);
    NSArray *thirteenthArray =[[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"URS",nil),NSLocalizedString(@"URS",nil),nil];
    NSString *fourteenDay = NSLocalizedString(@"BAHRIAM",nil);
    NSString *fifteenthDay = NSLocalizedString(@"URS",nil);
    NSArray *seventeenthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"URS",nil),NSLocalizedString(@"URS",nil),
                                 NSLocalizedString(@"URS",nil),NSLocalizedString(@"URS",nil),nil];
    NSString *eighteenDay =NSLocalizedString(@"URS",nil);
    NSArray *twentysecondArray = [[NSArray alloc] initWithObjects:
                              NSLocalizedString(@"URS",nil),NSLocalizedString(@"URS",nil),nil];
    NSString *twentySixthDay = NSLocalizedString(@"URS",nil);
    NSString *twentyEightthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects: dayOne, forthDay, fifthDay,
                       seventhDay, eighthDay, tenthDay, eleventhDay,
                       thirteenthArray, fourteenDay, fifteenthDay, seventeenthArray, eighteenDay,
                       twentysecondArray, twentySixthDay, twentyEightthDay, nil];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects:@"01", @"04", @"05",
                     @"06", @"08", @"10", @"11",
                     @"13", @"14", @"15", @"17", @"18",
                     @"22", @"26", @"28", nil];
    return keys;
}



@end
