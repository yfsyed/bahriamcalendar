//
//  MICCLunarDictionary_ZilHajj.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/21/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import "MICCLunarDictionary.h"
#import "LunarMonths.h"
#import "MuharramEventsList.h"
#import "ZilHajjEventsList.h"
#import "SafarEventList.h"
#import "ZiqadEventList.h"
#import "ShawwalEventList.h"
#import "RamzanEventList.h"
#import "RabbilAwwalEventList.h"
#import "RabisSaniEventList.h"
#import "JamadilAwwalEventList.h"
#import "JamadisSaniEventList.h"
#import "RajjabEventList.h"
#import "ShabaanEventList.h"

@implementation MICCLunarDictionary

-(NSDictionary *) eventsDictionaryFor:(int)month{
    NSDictionary *dictionary;
    
    switch(month){
        case MUHARRAM:
            dictionary = [MuharramEventsList eventDictionary];
            break;
        case SAFAR:
            dictionary = [SafarEventList eventDictionary];
            break;
        case RABIL_AWWAL:
            dictionary = [RabbilAwwalEventList eventDictionary];
            break;
        case RABIS_SANI:
            dictionary = [RabisSaniEventList eventDictionary];
            break;
        case JAMADIL_AWWAL:
            dictionary = [JamadilAwwalEventList eventDictionary];
            break;
        case JAMADIS_SANI:
            dictionary = [JamadisSaniEventList eventDictionary];
            break;
        case RAJJAB:
            dictionary = [RajjabEventList eventDictionary];
            break;
        case SHABAAN:
            dictionary = [ShabaanEventList eventDictionary];
            break;
        case RAMZAN:
            dictionary = [RamzanEventList eventDictionary];
            break;
        case SHAWWAL:
            dictionary = [ShawwalEventList eventDictionary];
            break;
        case ZIQAD:
            dictionary = [ZiqadEventList eventDictionary];
            break;
        case ZILHAJJ:
            dictionary = [ZilHajjEventsList eventDictionary];
            break;
            
    }
    return dictionary;
}

-(NSDictionary *) eventTypeDictionaryFor:(int)month{
    NSDictionary *dictionary;
    
    switch(month){
        case MUHARRAM:
            dictionary = [MuharramEventsList eventTypeDictionary];
            break;
        case SAFAR:
            dictionary = [SafarEventList eventTypeDictionary];
            break;
        case RABIL_AWWAL:
            dictionary = [RabbilAwwalEventList eventTypeDictionary];
            break;
        case RABIS_SANI:
            dictionary = [RabisSaniEventList eventTypeDictionary];
            break;
        case JAMADIL_AWWAL:
            dictionary = [JamadilAwwalEventList eventTypeDictionary];
            break;
        case JAMADIS_SANI:
            dictionary = [JamadisSaniEventList eventTypeDictionary];
            break;
        case RAJJAB:
            dictionary = [RajjabEventList eventTypeDictionary];
            break;
        case SHABAAN:
            dictionary = [ShabaanEventList eventTypeDictionary];
            break;
        case RAMZAN:
            dictionary = [RamzanEventList eventTypeDictionary];
            break;
        case SHAWWAL:
            dictionary = [ShawwalEventList eventTypeDictionary];
            break;
        case ZIQAD:
            dictionary = [ZiqadEventList eventTypeDictionary];
            break;
        case ZILHAJJ:
            dictionary = [ZilHajjEventsList eventTypeDictionary];
            break;
            
    }
    return dictionary;
}

-(NSArray *)eventsKeysFor:(int)month{
    NSArray *keysArray;
    
    switch(month){
        case MUHARRAM:
            keysArray = [MuharramEventsList eventsKeys];
            break;
        case SAFAR:
            keysArray = [SafarEventList eventsKeys];
            break;
        case RABIL_AWWAL:
            keysArray = [RabbilAwwalEventList eventsKeys];
            break;
        case RABIS_SANI:
            keysArray = [RabisSaniEventList eventsKeys];
            break;
        case JAMADIL_AWWAL:
            keysArray = [JamadilAwwalEventList eventsKeys];
            break;
        case JAMADIS_SANI:
            keysArray = [JamadisSaniEventList eventsKeys];
            break;
        case RAJJAB:
            keysArray = [RajjabEventList eventsKeys];
            break;
        case SHABAAN:
            keysArray = [ShabaanEventList eventsKeys];
            break;
        case RAMZAN:
            keysArray = [RamzanEventList eventsKeys];
            break;
        case SHAWWAL:
            keysArray = [ShawwalEventList eventsKeys];
            break;
        case ZIQAD:
            keysArray = [ZiqadEventList eventsKeys];
            break;
        case ZILHAJJ:
            keysArray = [ZilHajjEventsList eventsKeys];
            break;
    }
    return keysArray;
}

@end
