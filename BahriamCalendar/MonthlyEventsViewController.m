//
//  MonthlyEventsViewController.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/21/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import "MonthlyEventsViewController.h"
#import "MICCLunarDictionary.h"
#import "LunarMonths.h"

@interface MonthlyEventsViewController () 
@property (nonatomic,strong) NSDictionary *monthlyEventsDictionary;
@property (nonatomic,strong) NSDictionary *monthlyEventTypeDictionary;
@property (nonatomic,strong) NSArray *monthlyEventsKeys;
@property (nonatomic) int index;
@end


@implementation MonthlyEventsViewController

-(void)setMonthIndex:(int)index{
    self.index = index;
}

-(NSString *) getCurrentMonthName{
    NSString *monthName;
    switch(self.index){
        case MUHARRAM:
            monthName = NSLocalizedString(@"MUHARRAM", nil);
            break;
        case SAFAR:
            monthName = NSLocalizedString(@"SAFAR", nil);
            break;
        case RABIL_AWWAL:
            monthName = NSLocalizedString(@"RABIL_AWWAL", nil);
            break;
        case RABIS_SANI:
            monthName = NSLocalizedString(@"RABIS_SANI", nil);
            break;
        case JAMADIL_AWWAL:
            monthName = NSLocalizedString(@"JAMADIL_AWWAL", nil);
            break;
        case JAMADIS_SANI:
            monthName = NSLocalizedString(@"JAMADIS_SANI", nil);
            break;
        case RAJJAB:
            monthName = NSLocalizedString(@"RAJJAB", nil);
            break;
        case SHABAAN:
            monthName = NSLocalizedString(@"SHABAAN", nil);
            break;
        case RAMZAN:
            monthName = NSLocalizedString(@"RAMZAN", nil);
            break;
        case SHAWWAL:
            monthName = NSLocalizedString(@"SHAWWAL", nil);
            break;
        case ZIQAD:
            monthName = NSLocalizedString(@"ZIQAD", nil);
            break;
        case ZILHAJJ:
            monthName = NSLocalizedString(@"ZILHAJJ", nil);
            break;
    }
    return monthName;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    MICCLunarDictionary *monthlyEvents = [[MICCLunarDictionary alloc] init];
    self.monthlyEventsDictionary = [monthlyEvents eventsDictionaryFor:self.index];
    self.monthlyEventsKeys = [monthlyEvents eventsKeysFor:self.index];
    self.monthlyEventTypeDictionary = [monthlyEvents eventTypeDictionaryFor:self.index];
    UINavigationController *navCon  = (UINavigationController*) [self.navigationController.viewControllers objectAtIndex:1];
    navCon.navigationItem.title = [self getCurrentMonthName];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSInteger sectionsInTable = [self.monthlyEventsDictionary count];
    return sectionsInTable;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id sectionRows = [self.monthlyEventsDictionary objectForKey:[self.monthlyEventsKeys objectAtIndex:section]];
    if([sectionRows isKindOfClass:[NSArray class]]) {
        return [(NSArray *)sectionRows count];
    }else if([sectionRows isKindOfClass:[NSString class]]) {
        return 1;
    }
    return 0;
}


-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *sectionName = [NSString stringWithFormat:NSLocalizedString(@"%@ %@",nil),
                             [self.monthlyEventsKeys objectAtIndex:section],
                             [self getCurrentMonthName]];
    return sectionName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"MonthCells";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if(!cell) cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MonthCells"];
    // Configure the cell...

    id event = [self.monthlyEventsDictionary objectForKey:[self.monthlyEventsKeys objectAtIndex:indexPath.section]];
    id eventType = [self.monthlyEventTypeDictionary objectForKey:[self.monthlyEventsKeys objectAtIndex:indexPath.section]];
    
    if([event isKindOfClass:[NSArray class]]) {
        cell.textLabel.text = [(NSArray *)event objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [(NSArray *)eventType objectAtIndex:indexPath.row];
    }else if([event isKindOfClass:[NSString class]]) {
        cell.textLabel.text = (NSString *)event;
        cell.detailTextLabel.text = (NSString *)eventType;
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
