//
//  MICCHijriCalendarViewController.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/20/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import "MICCHijriCalendarViewController.h"
#import "MonthlyEventsViewController.h"

@interface MICCHijriCalendarViewController () <UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) NSArray *lunarMonth;
@end

@implementation MICCHijriCalendarViewController 

-(NSArray *) lunarMonth {
    if(!_lunarMonth) _lunarMonth = [[NSArray alloc] initWithObjects:
                                    NSLocalizedString(@"MUHARRAM",nil),
                                    NSLocalizedString(@"SAFAR",nil),
                                    NSLocalizedString(@"RABIL_AWWAL",nil),
                                    NSLocalizedString(@"RABIS_SANI",nil),
                                    NSLocalizedString(@"JAMADIL_AWWAL",nil),
                                    NSLocalizedString(@"JAMADIS_SANI",nil),
                                    NSLocalizedString(@"RAJJAB",nil),
                                    NSLocalizedString(@"SHABAAN",nil),
                                    NSLocalizedString(@"RAMZAN",nil),
                                    NSLocalizedString(@"SHAWWAL",nil),
                                    NSLocalizedString(@"ZIQAD",nil),
                                    NSLocalizedString(@"ZILHAJJ",nil),
                                    nil];
    return _lunarMonth;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.lunarMonthTableView.delegate = self;
    self.lunarMonthTableView.dataSource = self;
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.lunarMonthTableView reloadData];
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.lunarMonth count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if(!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = [self.lunarMonth objectAtIndex:indexPath.row];
    cell.tag = indexPath.row;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    UITableViewCell *cell = (UITableViewCell *)sender;
    NSInteger index = cell.tag;
    if([segue.identifier isEqualToString:@"events"]){
        [segue.destinationViewController setMonthIndex:(int)index];
    }
}

@end
