//
//  MICCViewController.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/20/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import "MICCViewController.h"

@interface MICCViewController (){
    int dayNum;
    BOOL isForward;
    int index;
    NSTimer *animationTimer;
}
@property (nonatomic,strong) NSArray *images;
@property (weak, nonatomic) IBOutlet UILabel *myDate;
@property (weak, nonatomic) IBOutlet UIImageView *firstImage;
@property (weak, nonatomic) IBOutlet UIImageView *secondImage;
@end

@implementation MICCViewController

-(NSArray *)images{
    if(!_images) {
        NSError *error;
        NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
        NSArray *resources = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:resourcePath error:&error];
        
        NSMutableArray *myPhotos = [[NSMutableArray alloc] init];
        for(NSString *path in resources) {
            if([path hasSuffix:@".jpg"]) {
                [myPhotos addObject:path];
            }
        }
        _images = [myPhotos mutableCopy];
    }
    return _images;
}

-(NSString *)getTodaysDateAsString{
    NSCalendar *islamicCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSIslamicCalendar];
    NSDate *today = [NSDate date];
    [islamicCalendar rangeOfUnit:NSDayCalendarUnit startDate:&today interval:NULL forDate:today];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setCalendar:islamicCalendar];
    [dateFormatter setTimeStyle:NSDateFormatterFullStyle];
    [dateFormatter setDateStyle:NSDateFormatterFullStyle];
    [dateFormatter setDateFormat:@"EEEE dd MMMM yyyy"];
    //english output
    //dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSString * islamicDateString = [dateFormatter stringFromDate:today];
    NSLog(@"%@", islamicDateString);
    return islamicDateString;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    dayNum=0;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    index = 0;
    isForward = YES;
    self.myDate.text = [self getTodaysDateAsString];
    [animationTimer invalidate];
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:8.0 target:self
                            selector:@selector(startAnimation) userInfo:nil repeats:YES];
    NSLog(@"refresh time");
}

-(void)viewWillDisappear:(BOOL)animated{
    [self stopAnimation];
}

-(void)stopAnimation{
    [animationTimer invalidate];
    animationTimer = nil;
    isForward = YES;
   // self.firstImage.image= nil;
   // self.secondImage.image= nil;
}

-(void) startAnimation
{
    if(self.images.count > 0){
        if(index >self.images.count-1) index = 0;
        
        UIImage *image = [UIImage imageNamed:[self.images objectAtIndex:index]];
        
        if(isForward){
            [self swapImageForward];
            self.secondImage.image = image;
        } else {
            [self swapImageBackword];
            self.firstImage.image = image;
        }
        index++;
    }
}

-(void)swapImageForward{
    isForward = NO;
    [UIView animateWithDuration:6 animations:^{
        self.firstImage.alpha=0.0;
        self.secondImage.alpha = 1.0;
    }];
}

-(void)swapImageBackword{
    isForward = YES;
    [UIView animateWithDuration:6 animations:^{
        self.secondImage.alpha = 0.0;
        self.firstImage.alpha=1.0;
    }];
}
@end
