//
//  ShawwalEventList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/17/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "ShawwalEventList.h"

@implementation ShawwalEventList

+(NSDictionary *) eventDictionary{
    
     NSArray *firstArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Imam Faqruddin Razi",nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Yaqoob Tawakkuli RH",nil),nil];

	 NSString *secondDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Yaqoob Tawakali RH",nil);
	 NSString *thirdDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Meeranji RH",nil);
	 NSString *forthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Mehmood RH",nil);

	 NSArray *fifthArray = [[NSArray alloc] initWithObjects:
            NSLocalizedString(@"Hzt Umm-al-Momineen Saudah RZ",nil),
            NSLocalizedString(@"Hzt Bandagi Miyan Syed Yaqoob Mahmoodi RH",nil),
            NSLocalizedString(@"Hzt Shah Raju Khataal RH",nil),nil];

	 NSArray *sixthArray = [[NSArray alloc] initWithObjects:
            NSLocalizedString(@"Hzt Bandagi Miyan Syed Mustafa Shaheed RH",nil),
                              NSLocalizedString(@"Hzt Bandagi Miyan Syed Mahmood RH",nil),nil];

	 NSString *seventhDay = NSLocalizedString(@"Bandagi Miyan Syed Mahmood RH",nil);
	 NSString *eighthDay = NSLocalizedString(@"Hzt Owais Qarni RZ",nil);

     NSString *eleventhDay = NSLocalizedString(@"Hazraat Ganj-e-Shuhada RZ",nil);
    
	 NSArray *twelthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hazraat Ganj Shuhada RZ",nil),
                            NSLocalizedString(@"Hzt Bibi Khunza Gouhar RZ",nil),nil];

	 NSString *thirteenthDay = NSLocalizedString(@"Syed-ush-Shuhada  Hzt Bandagi Miyan Syed Khundmir Siddiq-e-Vilayat RZ",nil);
	 NSString *fourteenthDay = NSLocalizedString(@"Mubarak Syed-ush-Shuhada Hzt Bandagi Miyan Syed Khundmir Siddiq-e-Vilayat RZ",nil);
	 NSString *fifteenthDay = NSLocalizedString(@"Bandagi Miyan Syed Yaqoob RH",nil);

	 NSArray *eighteenthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Amir Khusro Dahlavi RH",nil),
                            NSLocalizedString(@"Syed Nematullah RH",nil),nil];

	 NSString *ninteenthDay = NSLocalizedString(@"Hzt Syed Yahya RH",nil);

	 NSArray *twentythArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Syed Yahya RH",nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Hussain Shaheed RH",nil),nil];

	 NSString *twentySecondDay = NSLocalizedString(@"Hzt Miyan Syed Ashraf RH",nil);
	 NSString *twentyForthDay = NSLocalizedString(@"Hzt Allama Bandagi Miyan Abdul Malik Sujawandi Aalim Billah RH",nil);

	 NSArray *twentyNinthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Syed Mahmood RH",nil),
                            NSLocalizedString(@"Hzt Syed Murtuza RH",nil),
                            NSLocalizedString(@"Hzt Syed Abdul Qader RH",nil),
                            NSLocalizedString(@"Hzt Syedanji Miyan RH",nil),nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects: firstArray, secondDay, thirdDay, forthDay, fifthArray,
                       sixthArray, seventhDay, eighthDay, eleventhDay, twelthArray, thirteenthDay, fourteenthDay,
                       fifteenthDay, eighteenthArray, ninteenthDay,
                       twentythArray, twentySecondDay, twentyForthDay,  twentyNinthArray, nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+(NSDictionary *) eventTypeDictionary{
    NSArray *firstArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"BAHRIAM",nil),nil];
    
    NSString *secondDay = NSLocalizedString(@"Urs-e-Mubarak",nil);
    NSString *thirdDay = NSLocalizedString(@"Urs",nil);
    NSString *forthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *fifthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *sixthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"BAHRIAM",nil),nil];
    
    NSString *seventhDay = NSLocalizedString(@"Urs",nil);
    NSString *eighthDay = NSLocalizedString(@"Urs",nil);
    
    NSString *eleventhDay = NSLocalizedString(@"BAHRIAM",nil);
    
    NSArray *twelthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs-e-Mubarak",nil),
                            NSLocalizedString(@"Urs ",nil),nil];
    
    NSString *thirteenthDay = NSLocalizedString(@"BAHRIAM",nil);
    NSString *fourteenthDay = NSLocalizedString(@"Urs-e-Mubarak",nil);
    NSString *fifteenthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *eighteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Urs",nil),
                                NSLocalizedString(@"Urs",nil),nil];
    
    NSString *ninteenthDay = NSLocalizedString(@"BAHRIAM",nil);
    
    NSArray *twentythArray = [[NSArray alloc] initWithObjects:
                              NSLocalizedString(@"Urs",nil),
                              NSLocalizedString(@"Urs",nil),nil];
    
    NSString *twentySecondDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyForthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *twentyNinthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects: firstArray, secondDay, thirdDay, forthDay, fifthArray,
                       sixthArray, seventhDay, eighthDay, eleventhDay, twelthArray, thirteenthDay, fourteenthDay,
                       fifteenthDay, eighteenthArray, ninteenthDay,
                       twentythArray, twentySecondDay, twentyForthDay,  twentyNinthArray, nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects: @"01", @"02", @"03", @"04", @"05", @"06",
                     @"07", @"08", @"11", @"12",@"13", @"14",
                     @"15", @"18", @"19", @"20", @"22", @"24", @"29", nil];
    return keys;
}


@end
