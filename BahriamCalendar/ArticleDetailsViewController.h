//
//  ArticleDetailsViewController.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/23/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LunarMonths.h"

@interface ArticleDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *myWebView;
@property (nonatomic) int articleIndex;
@end
