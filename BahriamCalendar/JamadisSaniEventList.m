//
//  JamadisSaniEventList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/18/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "JamadisSaniEventList.h"

@implementation JamadisSaniEventList

+(NSDictionary *) eventDictionary{
    NSString *firstDay = NSLocalizedString(@"Hzt Miyan Syed Hussain RH",nil);
    NSString *thirdDay = NSLocalizedString(@"Hzt Miyan Syed Shareef RH",nil);
    NSString *forthDay = NSLocalizedString(@"Hzt Khuda-Baksh Khudawand RH",nil);
    NSString *fifthDay = NSLocalizedString(@"Hzt Moulana Jalaluddin Rumi RH",nil);
    NSString *sixthDay = NSLocalizedString(@"Hzt Saaduddin Kashgari RH",nil);
    NSString *eightDay = NSLocalizedString(@"Hzt Ali Maqdoom Mahimi RH",nil);
    NSString *twelveDay = NSLocalizedString(@"Hzt Miyan Syed Shah Muhammad RH",nil);
    
    NSArray *fourteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Imam Ghazali RH",nil),
                                NSLocalizedString(@"Hzt Imam Muhammad Shaibani",nil),nil];
    
    NSString *sixteenthDay = NSLocalizedString(@"Hzt Miyan Shah Ataullah RH",nil);
    NSString *seventeenthDay = NSLocalizedString(@"Hzt Shah Shareef RH Majzub Naousha",nil);
    NSString *nineteenthDay = NSLocalizedString(@"Hzt Miyan Shah Abdul Fatah RZ",nil);
    
    NSArray *twentyArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Miyan Shah Abdul Fatah RZ",nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Ali Shaheed RZ",nil),nil];
    
    NSArray *twentySecondArray = [[NSArray alloc] initWithObjects:
                                  NSLocalizedString(@"Hzt Ameer-ul-Momin Abu Bakar Siddiq RZ",nil),
                                  NSLocalizedString(@"Hzt Bandagi Miyan Syed Fareed Mohammad RH",nil),
                                  NSLocalizedString(@"Hzt Bandagi Miyan Mahmood Shah RH",nil),nil];
    
    NSString *twentyThirdDay = NSLocalizedString(@"Hzt Miyan Syed Zain-ul-abeddin RH",nil);
    NSString *twentyFifthDay = NSLocalizedString(@"Hzt Miyan Malik Burhanuddin RH",nil);
    
    NSArray *twentySixthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Miyan Syed Ishaq Ghazi RH",nil),
                                 NSLocalizedString(@"Hzt Miyan Syed Malik Ahmed RH",nil),nil];
    
    NSString *twentySeventhDay = NSLocalizedString(@"Hzt Bandagi Miyan Abdul Fatah RH",nil);
    NSString *twentyEighthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Yadullah RH",nil);
    NSString *twentyNinthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Yadullah RH alias Badi Shah Miyan Saheb",nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       firstDay, thirdDay, forthDay, fifthDay,sixthDay,
                       eightDay, twelveDay,fourteenthArray,sixteenthDay,seventeenthDay,nineteenthDay,
                       twentyArray,twentySecondArray,twentyThirdDay,twentyFifthDay,twentySixthArray,
                       twentySeventhDay, twentyEighthDay,twentyNinthDay,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+(NSDictionary *) eventTypeDictionary{
    NSString *firstDay = NSLocalizedString(@"Urs",nil);
    NSString *thirdDay = NSLocalizedString(@"Urs",nil);
    NSString *forthDay = NSLocalizedString(@"Urs",nil);
    NSString *fifthDay = NSLocalizedString(@"Urs",nil);
    NSString *sixthDay = NSLocalizedString(@"Urs",nil);
    NSString *eightDay = NSLocalizedString(@"Urs",nil);
    NSString *twelveDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *fourteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Urs",nil),
                                NSLocalizedString(@"Urs",nil),nil];
    
    NSString *sixteenthDay = NSLocalizedString(@"Urs",nil);
    NSString *seventeenthDay = NSLocalizedString(@"Urs",nil);
    NSString *nineteenthDay = NSLocalizedString(@"Bahr-e-Aam",nil);
    
    NSArray *twentyArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs",nil),
                            NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *twentySecondArray = [[NSArray alloc] initWithObjects:
                                  NSLocalizedString(@"Urs",nil),
                                  NSLocalizedString(@"Urs",nil),
                                  NSLocalizedString(@"Urs",nil),nil];
    
    NSString *twentyThirdDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyFifthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *twentySixthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),nil];
    
    NSString *twentySeventhDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyEighthDay = NSLocalizedString(@"Bahr-e-Aam",nil);
    NSString *twentyNinthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       firstDay, thirdDay, forthDay, fifthDay,sixthDay,
                       eightDay, twelveDay,fourteenthArray,sixteenthDay,seventeenthDay,nineteenthDay,
                       twentyArray,twentySecondArray,twentyThirdDay,twentyFifthDay,twentySixthArray,
                       twentySeventhDay, twentyEighthDay,twentyNinthDay,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+ (NSArray *)eventsKeys{
        NSArray *keys = [[NSArray alloc] initWithObjects:
                         @"01", @"03", @"04", @"05", @"06",
                         @"08", @"12", @"14", @"16", @"17", @"19",
                         @"20", @"22", @"23", @"25", @"26",
                         @"27", @"28", @"29", nil];
        return keys;
    }
@end
