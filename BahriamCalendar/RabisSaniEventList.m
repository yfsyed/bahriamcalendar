//
//  RabisSaniEventList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/18/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "RabisSaniEventList.h"

@implementation RabisSaniEventList

+(NSDictionary *) eventDictionary{
    
    NSString *thirdDay = NSLocalizedString(@"Hzt Umm-ul-Momineen Umm-e-Salma RZ",nil);
    NSString *sixthDay = NSLocalizedString(@"Hzt Miyan Syed Yaqoob RH",nil);
    
    NSArray *seventhArray = [[NSArray alloc] initWithObjects:
                             NSLocalizedString(@"Umm-ul-Bashar Bibi Hawa AHS",nil),
                             NSLocalizedString(@"Hzt Imam Malik RH",nil),
                             NSLocalizedString(@"Miyan Syed Ahmed Ghazi RH",nil),nil];
    
    NSArray *eighthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Miyan Syed Ahmed Ghazi RH",nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Allahdad Hameed RZ Sahabi-e-Mahdi-e-Mauod AHS",nil),nil];
    
    NSString *ninthDay = NSLocalizedString(@"Hzt Bandagi Miyan Allahdad Hameed RZ Sahabi-e-Mahdi-e-Mauod AHS",nil);
    NSString *tenthDay = NSLocalizedString(@"Hzt Imam Ahmed bin Hanbal RH 241H",nil);
    
    NSArray *eleventhArray = [[NSArray alloc] initWithObjects:
                              NSLocalizedString(@"Hzt Abdul Qader Jilani RH",nil),
                              NSLocalizedString(@"Hzt Syed Zain-ul-Aabideen RH",nil),nil];
    
    NSArray *twelthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Bibi Amina RH alias Aaqa Malik Mother of Hzt Imamuna Mahdi-e-Mauod AHS",nil),
                            NSLocalizedString(@"Hzt Allama Syed Nusrath RH",nil),nil];
    
    NSArray *seventeenArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Hzt Nizamuddin Auliya RH",nil),
                               NSLocalizedString(@"Hzt Miyan Syed Yousuf RH",nil),nil];
    
    NSString *eighteenDay = NSLocalizedString(@"Hzt Miyan Syed Abdul Qader RH",nil);
    
    NSArray *nineteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Miyan Syed Abdul Qader RH",nil),
                                NSLocalizedString(@"Hzt Syed Khundmir RH",nil),nil];
    
    NSString *twentySecondDay = NSLocalizedString(@"Shaikh-ul-Akbar Hzt Mohiuddin ibn Arabi RH Al Mubashir Mahdi AHS Pahlvan-e-Deen",nil);
    NSString *twentyFifthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Raj-u-Shaheed RH Ma'Deegar Shuhadah RH",nil);
    NSString *twentySixthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Abu Bakar RH",nil);
    NSString *twentyEighthDay = NSLocalizedString(@"Hzt Miyan Syed Allah Baksh RH",nil);
    
    NSArray *twentyNinthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Miyan Syed Allah Baksh RH",nil),
                                 NSLocalizedString(@"Hzt Khwaja Sufi Hameed Uddin RH",nil),nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       thirdDay,sixthDay,seventhArray,eighthArray,ninthDay,tenthDay,
                       eleventhArray,twelthArray,seventeenArray,eighteenDay,nineteenthArray,
                       twentySecondDay,twentyFifthDay,twentySixthDay,twentyEighthDay,twentyNinthArray,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
    

}

+(NSDictionary *) eventTypeDictionary{
    NSString *thirdDay = NSLocalizedString(@"Urs",nil);
    NSString *sixthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *seventhArray = [[NSArray alloc] initWithObjects:
                             NSLocalizedString(@"Urs",nil),
                             NSLocalizedString(@"Urs",nil),
                             NSLocalizedString(@"Bahr-e-Aam",nil),nil];
    
    NSArray *eighthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs",nil),
                            NSLocalizedString(@"Bahr-e-Aam",nil),nil];
    
    NSString *ninthDay = NSLocalizedString(@"Urs",nil);
    NSString *tenthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *eleventhArray = [[NSArray alloc] initWithObjects:
                              NSLocalizedString(@"Urs",nil),
                              NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *twelthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs",nil),
                            NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *seventeenArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Urs",nil),
                               NSLocalizedString(@"Urs",nil),nil];
    
    NSString *eighteenDay = NSLocalizedString(@"Bahr-e-Aam",nil);
    
    NSArray *nineteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Urs",nil),
                                NSLocalizedString(@"Urs",nil),nil];
    
    NSString *twentySecondDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyFifthDay = NSLocalizedString(@"Urs",nil);
    NSString *twentySixthDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyEighthDay = NSLocalizedString(@"Bahr-e-Aam",nil);
    
    NSArray *twentyNinthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       thirdDay,sixthDay,seventhArray,eighthArray,ninthDay,tenthDay,
                       eleventhArray,twelthArray,seventeenArray,eighteenDay,nineteenthArray,
                       twentySecondDay,twentyFifthDay,twentySixthDay,twentyEighthDay,twentyNinthArray,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
    

}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects:
                     @"03", @"06", @"07", @"08", @"09", @"10",
                     @"11", @"12", @"17", @"18", @"19",
                     @"22", @"25", @"26", @"28", @"29", nil];
    return keys;
}

@end
