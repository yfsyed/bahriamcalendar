//
//  MonthlyEventsViewController.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/21/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonthlyEventsViewController : UITableViewController <UITableViewDataSource,UITableViewDelegate>
-(void)setMonthIndex:(int)index;
@end
