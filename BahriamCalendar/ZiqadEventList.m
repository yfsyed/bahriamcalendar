//
//  ZiqadEventList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/17/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "ZiqadEventList.h"

@implementation ZiqadEventList



+(NSDictionary *) eventDictionary{
    
    NSString *dayOne = NSLocalizedString(@"Hzt Bandagi Miyan Shah Dilawar RZ",nil);
    NSString *secondDay = NSLocalizedString(@"Hzt Bandagi Miyan Shah Dilawar RZ",nil);
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Bandagi Miyan Syed Qaden RH",nil),
                                NSLocalizedString(@"Hzt Bandagi Miyan Syed Khundmir RH",nil),nil];
    
    NSArray *forthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Bandagi Miyan Mubarak Bhai Muhajir RZ",nil),
                                NSLocalizedString(@"Hzt Bandagi Miyan Syed Ishaq Bacha Miyan Majzoob RH",nil),nil];
    
    NSString *fifthDay = NSLocalizedString(@"Hzt Bandagi Miyan Mubarak Bhai Muhajir RZ",nil);
    NSString *seventhDay = NSLocalizedString(@"Hzt Bandagi Miyan Shah Nizam RZ",nil);
    NSString *eighthDay = NSLocalizedString(@"Hzt Bandagi Miyan Shah Nizam RZ",nil);
    NSString *tenthDay = NSLocalizedString(@"Hzt Bandagi Miyan Shah Abdul Karim Noori Shaheed RH",nil);
    NSString *eleventhDay = NSLocalizedString(@"Hzt Bandagi Miyan Shah Abdul Karim Noori Shaheed RH",nil);
    
    NSArray *sixteenthArray = [[NSArray alloc] initWithObjects:
                                    NSLocalizedString(@"Hzt Bandagi Miyan Syed Noor Muhammad RH",nil),
                                    NSLocalizedString(@"Hzt Syed Muhammad Gesudaraz",nil),nil];
    
    NSString *seventeenthDay = NSLocalizedString(@"Hzt Malik Sulaiman RH",nil);
    NSString *eighteenthDay = NSLocalizedString(@"Hzt Imamuna Miran Syed Muhammad Jaunpuri Mahdi-e-Maud Khalifatullah AHS",nil);
    
    NSArray *ninteenthArray = [[NSArray alloc] initWithObjects:
                                    NSLocalizedString(@"Hzt Imamuna Miran Syed Muhammad Jaunpuri Mahdi-e-Maud Khalifatullah AHS",nil),
                                    NSLocalizedString(@"Hzt Bandagi Miyan Shaikh Mustafa Gujarati RH",nil),nil];
    
    NSString *twentythDay = NSLocalizedString(@"Hzt Miyan Syed Mubarak RH",nil);
    NSString *twentyfirstDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Ishaq Bara bani Israil RH",nil);
    NSString *twentysecondDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Ishaq Bara bani Israil RH",nil);
    NSString *twentythirdDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Ashraf RH",nil);
    NSString *twentyfifthDay = NSLocalizedString(@"Hzt Miyan Syed Ashraf RH",nil);
    NSString *twentysixthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Burhan RH",nil);
    
    NSArray *twentyseventhArray = [[NSArray alloc] initWithObjects:
                                        NSLocalizedString(@"Hzt Bandagi Miyan Syed Mustafa Barah-Bani-Israil RH",nil),
                                        NSLocalizedString(@"Miyan Syed Abdul Hai Shah Shah Miyan Sahab Rh",nil),nil];
    
    NSArray *twentyninthArray = [[NSArray alloc] initWithObjects:
                                      NSLocalizedString(@"Hzt Miyan Syed Ibrahim RH",nil),
                                      NSLocalizedString(@"Hzt Bandagi Miyan Syed Peer Muhammad RH",nil),nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects: dayOne, secondDay, thirdArray, forthArray, fifthDay,
                       seventhDay, eighthDay, tenthDay, eleventhDay, sixteenthArray,
                       seventeenthDay, eighteenthDay, ninteenthArray, twentythDay,
                       twentyfirstDay, twentysecondDay, twentythirdDay, twentyfifthDay,
                       twentysixthDay, twentyseventhArray, twentyninthArray, nil];

    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}


+(NSDictionary *) eventTypeDictionary{
    
    NSString *dayOne = NSLocalizedString(@"BAHRIAM",nil);
    NSString *secondDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *forthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"BAHRIAM",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    NSString *fifthDay = NSLocalizedString(@"Urs",nil);
    NSString *seventhDay = NSLocalizedString(@"BAHRIAM",nil);
    NSString *eighthDay = NSLocalizedString(@"Urs",nil);
    NSString *tenthDay = NSLocalizedString(@"BAHRIAM",nil);
    NSString *eleventhDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *sixteenthArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Urs",nil),
                               NSLocalizedString(@"Urs",nil),nil];
    
    NSString *seventeenthDay = NSLocalizedString(@"Urs",nil);
    NSString *eighteenthDay = NSLocalizedString(@"BAHRIAM",nil);
    
    NSArray *ninteenthArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Urs",nil),
                               NSLocalizedString(@"Urs",nil),nil];
    
    NSString *twentythDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyfirstDay = NSLocalizedString(@"BAHRIAM",nil);
    NSString *twentysecondDay = NSLocalizedString(@"Urs",nil);
    NSString *twentythirdDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyfifthDay = NSLocalizedString(@"Urs",nil);
    NSString *twentysixthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *twentyseventhArray = [[NSArray alloc] initWithObjects:
                                   NSLocalizedString(@"Urs",nil),
                                   NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *twentyninthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),nil];
    
    
    NSArray *values = [[NSArray alloc] initWithObjects: dayOne, secondDay, thirdArray, forthArray, fifthDay,
                       seventhDay, eighthDay, tenthDay, eleventhDay, sixteenthArray,
                       seventeenthDay, eighteenthDay, ninteenthArray, twentythDay,
                       twentyfirstDay, twentysecondDay, twentythirdDay, twentyfifthDay,
                       twentysixthDay, twentyseventhArray, twentyninthArray, nil];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects: @"01", @"02", @"03", @"04", @"05",
                     @"07", @"08", @"10", @"11", @"16",
                     @"17", @"18", @"19", @"20",
                     @"21", @"22", @"23", @"25",
                     @"26", @"27", @"29", nil];
    return keys;
}

@end
