//
//  MICCLunarDictionary_ZilHajj.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/21/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MICCLunarDictionary : NSObject

/*
 * Important monthly events for a month.
 *
 */
-(NSDictionary *)eventsDictionaryFor:(int) month;


/*
 */
-(NSDictionary *) eventTypeDictionaryFor:(int)month;
    
/**
 *
 **/
-(NSArray *)eventsKeysFor:(int) month;
@end
