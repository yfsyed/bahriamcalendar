//
//  Header.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/21/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#ifndef BahriamCalendar_Header_h
#define BahriamCalendar_Header_h

#endif

typedef enum{
    MUHARRAM = 0,
    SAFAR,
    RABIL_AWWAL,
    RABIS_SANI,
    JAMADIL_AWWAL,
    JAMADIS_SANI,
    RAJJAB,
    SHABAAN,
    RAMZAN,
    SHAWWAL,
    ZIQAD,
    ZILHAJJ
} LunarMonths;

typedef enum{
    ARTICLES_OF_FAITH,
    IMAN,
    IHSAAN,
    ZAKAT,
    THE_PATHWAY_TO_ALLAH
} Articles;
