//
//  JamadilAwwalEventList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/18/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "JamadilAwwalEventList.h"

@implementation JamadilAwwalEventList


+(NSDictionary *) eventDictionary{
    NSArray *sixthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Khwaja Ibrahim Adham RH",nil),
                           NSLocalizedString(@"Hzt Miyan Syed Wali RH",nil),nil];
    
    NSString *eighthDay = NSLocalizedString(@"Hzt Miyan Sher Muhammad RH",nil);
    
    NSArray *tenthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Najamuddin Kibriya RH",nil),
                           NSLocalizedString(@"Hzt Bandagi Miyan Wazeeruddin RH",nil),
                           NSLocalizedString(@"Hzt Abu Bakar Ahmad RH",nil),nil];
    
    NSString *eleventhDay = NSLocalizedString(@"Hzt Bandagi Miyan Shah Sadiq Muhammad RH",nil);
    NSString *twelthDay = NSLocalizedString(@"Hzt Bandagi Miyan Shah Chand Muhammad RH",nil);
    
    NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Bandagi Miyan Khund Sayeed RH",nil),
                                NSLocalizedString(@"Hzt Miyan Syed Fazlullah RH",nil),
                                NSLocalizedString(@"Hzt Miyan Syed Shah Abdul Kareem RH",nil),
                                NSLocalizedString(@"Hzt Syed Miran Ghazi RH",nil),nil];
    
    NSString *fourteenthDay = NSLocalizedString(@"Hazrat Imamuna Miran Syed Muhammad Jaunpuri Mahdi-e-Ma'ud AHS Khatim-e-Vilyat-e-Muhammadia SAS",nil);
    NSString *fifteenthDay = NSLocalizedString(@"Hzt Syed Yousuf Khwaja Zade Miyan RH",nil);
    NSString *sixteenthDay = NSLocalizedString(@"Hzt Miyan Syed Abdul Hayi RH",nil);
    NSString *seventeenthDay = NSLocalizedString(@"Hzt Syed Badiuddin Zinda Shah Madar RH",nil);
    NSString *eighteenthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Shahab-ul-Haq RZ",nil);
    NSString *twentyFirstDay = NSLocalizedString(@"Hzt Syed Jalal RZ",nil);
    NSString *twentyThirdDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Allah Baksh RH Shaheed",nil);
    NSString *twentyForthDay = NSLocalizedString(@"Miyan Syed Yakoob Min Sahah Miyan RH",nil);
    NSString *twentyfifthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Hussain RH",nil);
    NSString *twentyEighthDay = NSLocalizedString(@"Hzt Miyan Syed Yahiya RH",nil);
    
    NSArray *twentyninthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Miyan Syed Moosa RH",nil),
                                 NSLocalizedString(@"Hzt Miyan Syed Hashim RH",nil),nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       sixthArray, eighthDay,  tenthArray,
                       eleventhDay, twelthDay, thirteenthArray, fourteenthDay,fifteenthDay, sixteenthDay, seventeenthDay, eighteenthDay,
                       twentyFirstDay, twentyThirdDay,twentyForthDay, twentyfifthDay, twentyEighthDay,  twentyninthArray,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;

}

+(NSDictionary *) eventTypeDictionary{
    NSArray *sixthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    NSString *eighthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *tenthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    NSString *eleventhDay = NSLocalizedString(@"Urs",nil);
    NSString *twelthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Urs",nil),
                                NSLocalizedString(@"Urs",nil),
                                NSLocalizedString(@"Urs",nil),
                                NSLocalizedString(@"Urs",nil),nil];
    
    NSString *fourteenthDay = NSLocalizedString(@"Milad",nil);
    NSString *fifteenthDay = NSLocalizedString(@"Urs",nil);
    NSString *sixteenthDay = NSLocalizedString(@"Urs",nil);
    NSString *seventeenthDay = NSLocalizedString(@"Urs",nil);
    NSString *eighteenthDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyFirstDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyThirdDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyForthDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyfifthDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyEighthDay = NSLocalizedString(@"Bahr-e-Aam",nil);
    
    NSArray *twentyninthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       sixthArray, eighthDay,  tenthArray,
                       eleventhDay, twelthDay, thirteenthArray, fourteenthDay,fifteenthDay, sixteenthDay, seventeenthDay, eighteenthDay,
                       twentyFirstDay, twentyThirdDay,twentyForthDay, twentyfifthDay, twentyEighthDay,  twentyninthArray,
                       nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;

}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects:
                     @"06", @"08", @"10",
                     @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18",
                     @"21", @"23", @"24", @"25", @"28", @"29",
                     nil];
    return keys;
}
@end
