//
//  MICCAppDelegate.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/20/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MICCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
