//
//  ZilHajjEventsList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/21/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import "ZilHajjEventsList.h"
#import "LunarMonths.h"

@implementation ZilHajjEventsList

+(NSDictionary *) eventDictionary{
    
    NSArray *secondArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"BIBI_Allahdadi_AZ",nil),
                            NSLocalizedString(@"BANDAGI_MIYAN_SYED_SHAREEF", nil),
                            nil];
    NSString *thirdDay = NSLocalizedString(@"BIBI_Allahdadi_AZ",nil);
    NSString *forthDay = NSLocalizedString(@"Hzt_Syed_Yaqoob",nil);
    NSArray *fifthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt_Miyan_Syed_Abdul_Wahaab",nil),
                           NSLocalizedString(@"HZT_MIYAN_SYED",nil),
                           nil];
    NSString *seventhDay = NSLocalizedString(@"Hzt Qutub Alam(RH)",nil);
    NSString *eighthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Khundmir(RH) [Barah Bani Israil]",nil);
    NSArray *ninthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"ARAFA",nil),
                           NSLocalizedString(@"Hzt Bandagi Miyan Aziz Muhammad",nil),
                           NSLocalizedString(@"HZT_Maulana_Abu_Sayeed_Syed_Mahmood",nil),
                           nil];
    NSArray *tenthArray = [[NSArray alloc] initWithObjects:
            NSLocalizedString(@"EID_UL_AZHA",nil), NSLocalizedString(@"HZT_Maulana_Abu_Sayeed_Syed_Mahmood",nil), nil];
    NSString *twelththDay = NSLocalizedString(@"Hzt_Bandagi_Miyan_Syed_Khundmir",nil);
    NSString *thirteenthDay =NSLocalizedString(@"Hzt_Bandagi_Miyan_Syed_Usman",nil);
    NSArray *fourteenArray = [[NSArray alloc] initWithObjects:
                              NSLocalizedString(@"Hzt_Bandagi_Miyan_Malik_Gouhar",nil),
                              NSLocalizedString(@"Hzt_Miyan_Syed_Jalal",nil),
                              nil];
    NSString *fifteenthDay = NSLocalizedString(@"Hzt_Bandagi_Miyan_Malik_Meeran",nil);
    NSString *seventeenthDay = NSLocalizedString(@"Hzt_Bandagi_Miyan_Syed_Ibrahim",nil);
    NSArray *eighteenArray = [[NSArray alloc] initWithObjects:
                              NSLocalizedString(@"Amir-ul-Momineen_Hzt_Osman-e-Ghani",nil),
                              NSLocalizedString(@"Hzt_Miyan_Hussain_Shah_Miyan",nil),
                              nil];
    
    NSString *twentyDay = NSLocalizedString(@"Hzt_Miyan_Abdul_Fatah",nil);
    NSString *twentysecondDay = NSLocalizedString(@"BAHRIAM_OF_Hzt_Bandagi_Shah_Yakoob",nil);
    NSString *twentyThirdDay = NSLocalizedString(@"Hzt_Bandagi_Shah_Yakoob",nil);
    NSString *twentyFifthDay = NSLocalizedString(@"Hzt_Syed_Shareef",nil);
    NSArray *twentySeventhArray = [[NSArray alloc] initWithObjects:
                                   NSLocalizedString(@"Hzt_Sheikh_Fariduddin_Athar",nil),
                                   NSLocalizedString(@"Hzt_Syed_Muhammad_Dara_Miyan",nil),
                                   nil];
    NSString *twentyEightthDay = NSLocalizedString(@"Hzt_Sheikh_Shebli",nil);
    NSArray *twentyNinthArray = [[NSArray alloc] initWithObjects:                                
                                 NSLocalizedString(@"Hzt_Miyan_Syed_Hashim", nil),
                                 NSLocalizedString(@"Hzt_Syed_Naji_Miyan", nil),
                                 nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects: secondArray, thirdDay, forthDay, fifthArray,
                       seventhDay, eighthDay, ninthArray, tenthArray, twelththDay,
                       thirteenthDay, fourteenArray, fifteenthDay, seventeenthDay, eighteenArray,
                       twentyDay, twentysecondDay, twentyThirdDay, twentyFifthDay, twentySeventhArray,
                       twentyEightthDay, twentyNinthArray, nil];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+(NSDictionary *) eventTypeDictionary{
    
    NSArray *secondArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"BAHRIAM",nil),
                            NSLocalizedString(@"URS", nil),
                            nil];
    
    
    NSString *thirdDay = NSLocalizedString(@"URS",nil);
    NSString *forthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *fifthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"URS",nil),
                           NSLocalizedString(@"URS",nil),
                           nil];
    
    NSString *seventhDay = NSLocalizedString(@"URS",nil);
    
    NSString *eighthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *ninthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"General",nil),
                           NSLocalizedString(@"URS",nil),
                           NSLocalizedString(@"BAHRIAM",nil),
                           nil];
    
    NSArray *tenthArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"General",nil), NSLocalizedString(@"URS",nil), nil];
    NSString *twelththDay = NSLocalizedString(@"URS",nil);
    NSString *thirteenthDay =NSLocalizedString(@"URS",nil);
    NSArray *fourteenArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"URS",nil), NSLocalizedString(@"URS",nil),nil];
    NSString *fifteenthDay = NSLocalizedString(@"URS",nil);
    NSString *seventeenthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *eighteenArray = [[NSArray alloc] initWithObjects:
                              NSLocalizedString(@"URS",nil),
                              NSLocalizedString(@"URS",nil),
                              nil];
    
    NSString *twentyDay = NSLocalizedString(@"URS",nil);
    
    NSString *twentysecondDay = NSLocalizedString(@"BAHRIAM",nil);
    NSString *twentyThirdDay = NSLocalizedString(@"URS",nil);
    NSString *twentyFifthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *twentySeventhArray = [[NSArray alloc] initWithObjects:
                                   NSLocalizedString(@"URS",nil),
                                   NSLocalizedString(@"URS",nil),
                                   nil];
    
    NSString *twentyEightthDay = NSLocalizedString(@"URS",nil);
    
    NSArray *twentyNinthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"URS", nil),
                                 NSLocalizedString(@"URS", nil),
                                 nil];
    
    
    
    NSArray *values = [[NSArray alloc] initWithObjects: secondArray, thirdDay, forthDay, fifthArray,
                       seventhDay, eighthDay, ninthArray, tenthArray, twelththDay,
                       thirteenthDay, fourteenArray, fifteenthDay, seventeenthDay, eighteenArray,
                       twentyDay, twentysecondDay, twentyThirdDay, twentyFifthDay, twentySeventhArray,
                       twentyEightthDay, twentyNinthArray, nil];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects:@"02", @"03", @"04", @"05",
                     @"07", @"08", @"09", @"10", @"12",
                     @"13", @"14", @"15", @"17", @"18",
                     @"20", @"22", @"23", @"25", @"27",
                     @"28", @"29", nil];
    return keys;
}

@end
