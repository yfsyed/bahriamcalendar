//
//  ZilHajjEventsList.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/21/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZilHajjEventsList : NSObject

+(NSDictionary *) eventDictionary;
+(NSDictionary *) eventTypeDictionary;
+(NSArray *) eventsKeys;

@end
