//
//  Safar.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/17/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SafarEventList : NSObject
+(NSDictionary *) eventDictionary;
+(NSDictionary *) eventTypeDictionary;
+(NSArray *) eventsKeys;
@end
