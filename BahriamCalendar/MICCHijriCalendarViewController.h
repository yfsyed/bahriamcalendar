//
//  MICCHijriCalendarViewController.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/20/13.
//  Copyright (c) 2013 Yousuf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MICCHijriCalendarViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *lunarMonthTableView;
@end
