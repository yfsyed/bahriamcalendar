//
//  JamadisSaniEventList.h
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/18/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JamadisSaniEventList : NSObject
+(NSDictionary *) eventDictionary;
+(NSDictionary *) eventTypeDictionary;
+(NSArray *) eventsKeys;
@end
