//
//  RabbilAwwalEventList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/18/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "RabbilAwwalEventList.h"

@implementation RabbilAwwalEventList



+(NSDictionary *) eventDictionary{
    NSArray *secondArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Khatim-e-Ambiya Hazrat Muhammad Mustafa Rasool Allah SAS",nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Ajmal RZ",nil),nil];
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Khwaja Bawauddin Naqashbandi RH",nil),
                           NSLocalizedString(@"Umm-ul-Musadaqeen Bibi Bounji RZ",nil),nil];
    
    NSArray *fourthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Bandagi Miyan Shah Abdul Momin RH",nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Shah Muhammad RH",nil),nil];
    
    NSString *fifthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Shah Muhammad RH",nil);
    NSString *seventhDay = NSLocalizedString(@"Hzt Khwaja Muntajibuddin Zarzari Zar Baksh Dulha RH",nil);
    
    NSArray *ninthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Bandagi Miyan LadShah RZ",nil),
                           NSLocalizedString(@"Umm-ul-Musadeqeen Bibi Malkan RZ",nil),
                           NSLocalizedString(@"Hzt Bandagi Miyan Shah Abdul Rahman RZ",nil),nil];
    
    NSString *tenthDay = NSLocalizedString(@"Hzt Syed Saadullah RH",nil);
    NSString *eleventhDay = NSLocalizedString(@"Hzt Shah Abdul Rahman Dulah Ghazi RH",nil);
    
    NSArray *twenthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Milad Khatim-ul-Anbiya Hazrat Muhammad Mustafa Rasool Allah SAS",nil),
                            NSLocalizedString(@"Hzt Bandagi Miyan Salah Muhammad RH",nil),nil];
    
    NSString *thirteenthDay = NSLocalizedString(@"Bibi Hadanji RZ  D/o Hzt Imamuna AHS",nil);
    NSString *fourteenthDay = NSLocalizedString(@"Hzt Khwaja Syed Muhammad Qutubuddin Bakhtiar Kaki RH",nil);
    NSString *sixteenthDay = NSLocalizedString(@"Hzt Bandagi Miyan Abdul Latif RH",nil);
    NSString *seventeenthDay = NSLocalizedString(@"Hzt Miyan Syed Ibrahim RH Alias Munawwar Sahab",nil);
    
    NSArray *eighteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Maqdoom Shah Daniyal Khizri RZ",nil),
                                NSLocalizedString(@"Hzt Miyan Syed Ibrahim RH Alias Munawwar Sahab",nil),nil];
    NSArray *ninteenthArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Hzt Miyan Syed Hussain RH",nil),
                               NSLocalizedString(@"Hzt Miyan Syed Alam RH",nil),nil];
    
    NSString *twentyDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Salaamullah RZ Sahabi-e-Mahdi-e-Mauod AHS",nil);
    NSString *twentySecondDay = NSLocalizedString(@"Hzt Miyan Syed Salaamullah RH",nil);
    NSString *twentyThirdDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Yaseen RH",nil);
    
    NSArray *twentyfifthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Syed Zain-ul-Aabideen Baais Khwaja RH",nil),
                                 NSLocalizedString(@"Hzt Miyan Syed Jalal RH",nil),
                                 NSLocalizedString(@"Hzt Miyan Syed Abdul Lateef RH",nil),nil];
    
    NSString *twentyEighthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Yahya",nil);
    
    NSArray *twentyninthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Umm-ul-Momineen Juveirah RZ",nil),
                                 NSLocalizedString(@"Hzt Maqdoom Haji Nizamuddin RH",nil),nil];
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                        secondArray, thirdArray, fourthArray, fifthDay,seventhDay,
                       ninthArray, tenthDay,
                       eleventhDay, twenthArray, thirteenthDay, fourteenthDay,
                       sixteenthDay,seventeenthDay,eighteenthArray,
                       ninteenthArray, twentyDay, twentySecondDay,
                       twentyThirdDay,twentyfifthArray,
                       twentyEighthDay,  twentyninthArray, nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+(NSDictionary *) eventTypeDictionary{
    NSArray *secondArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs-e-Mubarrak",nil),
                            NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *fourthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs",nil),
                            NSLocalizedString(@"Bahr-e-Aam",nil),nil];
    
    NSString *fifthDay = NSLocalizedString(@"Urs",nil);
    NSString *seventhDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *ninthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    NSString *tenthDay = NSLocalizedString(@"Urs",nil);
    NSString *eleventhDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *twenthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs",nil),
                            NSLocalizedString(@"Urs",nil),nil];
    
    NSString *thirteenthDay = NSLocalizedString(@"Urs",nil);
    NSString *fourteenthDay = NSLocalizedString(@"Urs",nil);
    NSString *sixteenthDay = NSLocalizedString(@"Urs",nil);
    NSString *seventeenthDay = NSLocalizedString(@"Bahr-e-Aam",nil);
    
    NSArray *eighteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Urs",nil),
                                NSLocalizedString(@"Urs",nil),nil];
    NSArray *ninteenthArray = [[NSArray alloc] initWithObjects:
                               NSLocalizedString(@"Urs",nil),
                               NSLocalizedString(@"Urs",nil),nil];
    
    NSString *twentyDay = NSLocalizedString(@"Urs",nil);
    NSString *twentySecondDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyThirdDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *twentyfifthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),nil];
    
    NSString *twentyEighthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *twentyninthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),nil];
    NSArray *values = [[NSArray alloc] initWithObjects:
                       secondArray, thirdArray, fourthArray, fifthDay,seventhDay,
                       ninthArray, tenthDay,
                       eleventhDay, twenthArray, thirteenthDay, fourteenthDay,
                       sixteenthDay,seventeenthDay,eighteenthArray,
                       ninteenthArray, twentyDay, twentySecondDay,
                       twentyThirdDay,twentyfifthArray,
                       twentyEighthDay,  twentyninthArray, nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects:
                     @"02", @"03", @"04", @"05",
                     @"07", @"09", @"10", @"11", @"12",@"13", @"14",
                     @"16", @"17", @"18", @"19",
                     @"20", @"22", @"23", @"25",
                     @"28", @"29",  nil];
    return keys;
}



@end
