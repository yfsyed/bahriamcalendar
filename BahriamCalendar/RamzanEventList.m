//
//  RamzanEventList.m
//  BahriamCalendar
//
//  Created by Yousuf Syed on 5/18/14.
//  Copyright (c) 2014 Yousuf. All rights reserved.
//

#import "RamzanEventList.h"

@implementation RamzanEventList

+(NSDictionary *) eventDictionary{
    NSString *firstDay = NSLocalizedString(@"Hzt Bibi Amtul Kareem RH",nil);
    
    
    NSArray *secondArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Hzt Bandagi Miyan Syed Abdul Hai",nil),
                            NSLocalizedString(@"Hzt Miyan Syed Manju RH",nil),nil];
    
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Siddiq-e-Akber Hzt Bandagi Miran Syed Mahmood Sani-e-Mehdi RZ",nil),
                           NSLocalizedString(@"Syeda-tun-Nisa Bibi Fatima-uz-Zehra RZ Khatoon-e-Jannat",nil),nil];
    
    NSString *forthDay = NSLocalizedString(@"Siddiq-e-Akber Hzt Bandagi Miran Syed Mahmood Sani-e-Mehdi RZ",nil);
    
    NSArray *fifthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Hzt Bandagi Miyan Syed Manju RH",nil),
                           NSLocalizedString(@"Hzt Saheba Khan Bibi Saheba RH",nil),nil];
    
    NSArray *sixthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Miyan Syed  Abdul Khader RH",nil),
                                NSLocalizedString(@"Hzt Miyan Syed Zain-ul-Abideen RH",nil),nil];
    
    NSString *seventhDay = NSLocalizedString(@"Hzt Bilal RZ",nil);
    
    NSArray *eighthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Miyan Syed Dilawar Mahdavi urf Gorey Miyan Saheb RH",nil),
                                 NSLocalizedString(@"Hzt Syed Muhammad Roshan Miyan Saheb RH",nil),nil];
    
    NSArray *ninthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Khwaja Habeeb Ujme RH",nil),
                                NSLocalizedString(@"Hzt Shafeeq Balqi RH",nil),nil];
    
    NSArray *tenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Umm-ul-Momineen Hzt Khadija-tul-Kubra RZ",nil),
                                NSLocalizedString(@"Hzt Khwaja Naseeruddin Chiragh Dehalwi RH",nil),
                                NSLocalizedString(@"Hzt Bandagi Miyan Tashreefullah RZ",nil),nil];
    
    
    NSString *eleventhDay = NSLocalizedString(@"Hzt Bandagi Miyan Tashreefullah RZ",nil);
    NSString *twelthDay = NSLocalizedString(@"Hzt Bandagi Malik Peer Mohammad RH",nil);
    
    NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                     NSLocalizedString(@"Hzt Bandgi Malik Ilahdad Khalifay-Groh RZ",nil),
                                     NSLocalizedString(@"Hzt Miyan Syed Mahmood RH",nil),
                                     NSLocalizedString(@"Hzt Bu Ali Shah Qalandar Sharfuddin RH",nil),nil];
    
    NSString *forteenthDay = NSLocalizedString(@"Hzt Bandagi Miyan Malik Ilahdad Khalifay Groh-e-Mahdavia",nil);
    
    NSArray *fifteenthArray = [[NSArray alloc] initWithObjects:
                                    NSLocalizedString(@"Hzt Syed Mosa RH",nil),
                                    NSLocalizedString(@"Hzt Syed Saadullah RH",nil),
                                    NSLocalizedString(@"Hzt Abubakr alias Bade Miyan RH",nil),nil];
    
    NSString *sixteenthDay = NSLocalizedString(@"Hzt Syed Salamullah RH",nil);
    NSString *seventeenthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Raj Mohammad RH",nil);
    NSString *eighteenthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Alam RH",nil);
    NSString *nineteenthDay = NSLocalizedString(@"Mubarak Hzt Bandagi Miyan Syed Alam RH",nil);
    
    NSString *twentythDay = NSLocalizedString(@"Ameer-ul-Momineen Hzt Syeduna Ali RZ",nil);
    NSString *twentyForthDay = NSLocalizedString(@"Hzt Bandagi Miyan Abdur-Rasheed RZ",nil);
    NSArray *twenthFifthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Hzt Bandagi Miyan Syed Abu Bakr RZ",nil),
                                 NSLocalizedString(@"Hzt Miyan Syed Isa Mehmood RH",nil),nil];
                                 
    NSArray *twenthSixthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"Hzt Ruknuddin Majzoob RH",nil),
                                NSLocalizedString(@"Hzt Miyan Syed Al-Hadad RH",nil),
                                NSLocalizedString(@"Hzt Miyan Syed Mubarak Shaheed RH",nil),nil];
                                 
    NSString *twentySeventhDay = NSLocalizedString(@"Hzt Miyan Syed Ashraf Khuzade Miyan RH",nil);
    NSString *twentyEighthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Alam RH",nil);
    NSString *twentyNinthDay = NSLocalizedString(@"Hzt Bandagi Miyan Syed Alam RH",nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       firstDay, secondArray, thirdArray, forthDay, fifthArray,sixthArray,
                       seventhDay, eighthArray, ninthArray, tenthArray,
                       eleventhDay, twelthDay, thirteenthArray, forteenthDay,
                       fifteenthArray,sixteenthDay,seventeenthDay,eighteenthDay,
                       nineteenthDay,twentythDay,twentyForthDay,
                       twenthFifthArray,twenthSixthArray, twentySeventhDay,
                       twentyEighthDay,  twentyNinthDay, nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;

}

+(NSDictionary *) eventTypeDictionary{
   
    NSString *firstDay = NSLocalizedString(@"Urs",nil);
    
    
    NSArray *secondArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs",nil),
                            NSLocalizedString(@"Urs",nil),nil];
    
    
    NSArray *thirdArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"BAHRIAM",nil),
                           NSLocalizedString(@"Urs-e-Mubarak",nil),nil];
    
    NSString *forthDay = NSLocalizedString(@"Urs-e-Mubarak",nil);
    
    NSArray *fifthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    
    NSArray *sixthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    NSString *seventhDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *eighthArray = [[NSArray alloc] initWithObjects:
                            NSLocalizedString(@"Urs",nil),
                            NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *ninthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *tenthArray = [[NSArray alloc] initWithObjects:
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"Urs",nil),
                           NSLocalizedString(@"BAHRIAM",nil),nil];
    
    
    NSString *eleventhDay = NSLocalizedString(@"Urs",nil);
    NSString *twelthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *thirteenthArray = [[NSArray alloc] initWithObjects:
                                NSLocalizedString(@"BAHRIAM",nil),
                                NSLocalizedString(@"Urs",nil),
                                NSLocalizedString(@"Urs",nil),nil];
    
    NSString *forteenthDay = NSLocalizedString(@"Urs",nil);
    
    
    NSArray *fifteenthArray = [[NSArray alloc] initWithObjects:
                                    NSLocalizedString(@"Urs",nil),
                                    NSLocalizedString(@"Urs",nil),
                                    NSLocalizedString(@"Urs",nil),nil];
    
    NSString *sixteenthDay = NSLocalizedString(@"Urs",nil);
    NSString *seventeenthDay = NSLocalizedString(@"Urs",nil);
    NSString *eighteenthDay = NSLocalizedString(@"BAHRIAM",nil);
    NSString *nineteenthDay = NSLocalizedString(@"Urs",nil);
    
    
    NSString *twentythDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyForthDay = NSLocalizedString(@"Urs",nil);
    
    NSArray *twenthFifthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),nil];
    
    NSArray *twenthSixthArray = [[NSArray alloc] initWithObjects:
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),
                                 NSLocalizedString(@"Urs",nil),nil];
    
    NSString *twentySeventhDay = NSLocalizedString(@"Urs",nil);
    NSString *twentyEighthDay = NSLocalizedString(@"BAHRIAM",nil);
    NSString *twentyNinthDay =   NSLocalizedString(@"Urs",nil);
    
    NSArray *values = [[NSArray alloc] initWithObjects:
                       firstDay, secondArray, thirdArray, forthDay, fifthArray,sixthArray,
                       seventhDay, eighthArray, ninthArray, tenthArray,
                       eleventhDay, twelthDay, thirteenthArray, forteenthDay,
                       fifteenthArray,sixteenthDay,seventeenthDay,eighteenthDay,
                       nineteenthDay, twentythDay, twentyForthDay,
                       twenthFifthArray,twenthSixthArray, twentySeventhDay,
                       twentyEighthDay,  twentyNinthDay, nil];
    
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithObjects:values forKeys:[self eventsKeys]];
    return dictionary;
    
}

+ (NSArray *)eventsKeys{
    NSArray *keys = [[NSArray alloc] initWithObjects: @"01", @"02", @"03", @"04", @"05", @"06",
                     @"07", @"08", @"09", @"10", @"11", @"12",@"13", @"14",
                     @"15", @"16", @"17", @"18", @"19",
                     @"20", @"24", @"25", @"26", @"27", @"28", @"29",  nil];
    return keys;
}


@end
